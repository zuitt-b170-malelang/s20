/* 
JSON(Javascript Object Notation)
-a data format used by applciations to store and transport data to one another
-even though it has the "Javascript" in its name, JsSON is not limited to Javascript-based applicatations and can also be used in other programming languages.
-Files that store JSON data are saved with a file extension of .json
*/
/* 
JS Object with the following property:
city
province
country
*/

/* 
DIFFERENCE:
  JS Object vs JSON

  1.
  // JS Object -has the value inserted quotation marks

  // JSON - quotation marks for both key and the value

  2.
  // JS Object - exclusive to Javascript

  // JSON - not exclusive for javascript. other programming languages can also use JSON files

*/
/* let city = {
  "city":"Antipolo",
  "province": "Rizal",
  "country": "Philippines"
};
console.log(city); */


// JSON Arrays
/* let cities = [
    {
      "city":"Antipolo",
      "province": "Rizal",
      "country": "Philippines"
    },
    {
      "city":"Bacoor",
      "province": "Cavite",
      "country": "Philippines"
    },
    {
      "city":"New York",
      "province": "New York",
      "country": "U.S.A."
    }

];
console.log(cities); */

// JSON Object
/* 
  JSON
  used for serializing/deserializing different data types into byte.

  Serialization - process of converting data into series of bytes for easier transmission or transfer of information

  byte - binary (1 & 0)
*/


// JSON METHODS

  // JSON object contains methods for parsing and converting data into stringified JSON
  // Stringified JSON - JSON Object/JS objects converted into string to be used in other functions of the language esp. Javascript-based applications(serialize)
let batches =[
  {
    "batchName": "Batch X"
  },
  {
    "batchName": "Batch Y"
  }
];
console.log("Result from console.log method");
console.log(batches);
console.log("Result from stringify method");
// stringify - convert JSON Objects into JSON (string)
console.log(JSON.stringify(batches));

let data = JSON.stringify({
  name:"John",
  age: 31,
  address:{
    city:"Manila",
    country: "Philippines"
  }
});
console.log(data);

/* Assignment
  create userDetails variable that will contain JS object with the ff properties:
  firstName - prompt
  lname - prompt
  age - prompt
  address:{
    city - prompt
    country - prompt
    zipCode - prompt
  }
  log in the console the converted JSON data type
*/

/* let userDetails = JSON.stringify({
  fname: prompt("Please enter your first name"),
  lname: prompt("Please enter your last name"),
  age: /* Number - no need *//* (prompt("Age"))(prompt("Age")),
  address:{
    city: prompt("City"),
    country: prompt("Country"),
    zipCode: prompt("Zipcode"),
  }
}); */
/* console.log(userDetails); */

// CONVERTING OF STRINGIFIED JASON INTO JS OBJECTS
  // Parse Method - converting a json data into js objects
  // information is commonly sent to application in STRINGIFIED JSON; then converted back into objects; this happens both for sending information to a backend app such as databases and back to frontend app such as the webpages.
  // upon receiving the data, JSON text can be converted into a JS/JSON Object with parse method
let batchesJSON = `[
  {
    "batchName": "Batch X"
  },
  {
    "batchName": "Batch Y"
  }
]`;
console.log(batchesJSON);
console.log("Result from parse method");
console.log(JSON.parse(batchesJSON));

let stringifiedData = `{
  "name":"John",
  "age": "31",
  "address":{
    "city":"Manila",
    "country": "Philippines"
  }
}`;
// how do we convert the JSON string above into a JS Object
console.log(JSON.parse(stringifiedData));